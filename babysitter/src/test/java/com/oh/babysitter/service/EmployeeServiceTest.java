package com.oh.babysitter.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.oh.babysitter.models.Babysitter;

/**
 * Employee Service test
 * 
 * @author Shiva Acharya
 *
 */
class EmployeeServiceTest {

	private EmployeeService employeeService;
	private Babysitter babysitter;

	// set up
	@BeforeEach
	public void setUp() {
		babysitter = new Babysitter();
		employeeService = new EmployeeService(babysitter);
	}

	// test constructor
	@Test
	public void testConstructor() {

		// check the employee and babysitter object for null
		assertNotNull(employeeService);
		assertNotNull(babysitter);

		// check the babysitter object and employee object are same
		assertSame(employeeService.getEmployee(), babysitter);

		// create new babysitter and test
		Babysitter babysitter2 = new Babysitter(1, 2, 3);

		// set new Babysitter to employee service
		employeeService.setEmployee(babysitter2);
		assertSame(employeeService.getEmployee(), babysitter2);
	}

	// test getters and setters
	@Test
	public void testGettersAndSetters() {

		// check the employee and babysitter object for null
		assertNotNull(employeeService);
		assertNotNull(babysitter);

		// check the babysitter object and employee object are same
		assertSame(employeeService.getEmployee(), babysitter);

		// check for values
		assertEquals(0, employeeService.getEmployee().getStartTime());
		assertEquals(0, employeeService.getEmployee().getBedTime());
		assertEquals(0, employeeService.getEmployee().getEndTime());

		// create new babysitter and test
		Babysitter babysitter2 = new Babysitter(1, 2, 3);

		// set new Babysitter to employee service
		employeeService.setEmployee(babysitter2);
		assertSame(employeeService.getEmployee(), babysitter2);

		// check for values
		assertEquals(1, employeeService.getEmployee().getStartTime());
		assertEquals(2, employeeService.getEmployee().getBedTime());
		assertEquals(3, employeeService.getEmployee().getEndTime());
	}

	// test null and default employee object passed in construtor's pay
	@Test
	public void testNullAndDefaultEmployeePay() {

		// check the employee and babysitter object for null
		assertNotNull(employeeService);
		assertNotNull(babysitter);

		// set employee to null and check the pay is 0
		employeeService.setEmployee(null);
		assertEquals(0, employeeService.calculatePay());
		
		employeeService.setEmployee(new Babysitter(1, 2, 3));
		assertNotEquals(0, employeeService.calculatePay());

	}

	// test the time entry entered are valid
	@Test
	public void testValidTimeEntry() {

		// check the employee and babysitter object for null
		assertNotNull(employeeService);
		assertNotNull(babysitter);
		
		//invalid time
		assertFalse(employeeService.isValidTimeEntry(0, 0, 0));
		//test couple more

		assertFalse(employeeService.isValidTimeEntry(0, 5, 8));
		assertFalse(employeeService.isValidTimeEntry(15, 3, 7));
		assertFalse(employeeService.isValidTimeEntry(21, 0, -3));
		assertFalse(employeeService.isValidTimeEntry(-10, 5, 7));
		
		//check valid time entry returns true
		assertTrue(employeeService.isValidTimeEntry(5, 10, 4));
		assertTrue(employeeService.isValidTimeEntry(3, 4, 1));
	}
	
	// test simple time entry data calculate pay
		@Test
		public void testCalculatePaySimple() {

			// check the employee and babysitter object for null
			assertNotNull(employeeService);
			assertNotNull(babysitter);
			
			//Check for invalid time entry for total pay of 0
			assertEquals(0, employeeService.calculatePay());
			
			//add Babysitter with invalid time entry and check pay is 0
			employeeService.setEmployee(new Babysitter(-3, 7, 10));
			assertEquals(0, employeeService.calculatePay());
			
			employeeService.setEmployee(new Babysitter(7, 19, 3));
			assertEquals(0, employeeService.calculatePay());
			
			employeeService.setEmployee(new Babysitter(6, 12, 0));
			assertEquals(0, employeeService.calculatePay());
			
			//test valid time entry
			Babysitter babysitter2 = new Babysitter(5, 10, 4);
			employeeService.setEmployee(babysitter2);
			//expected should 140
			assertEquals(140, employeeService.calculatePay());
			
			//set babysitter start time to some other number and check the pay
			babysitter2.setStartTime(9); //(140 - 48 = 92 should be new expected
			employeeService.setEmployee(babysitter2);
			assertEquals(92, employeeService.calculatePay());
			
			//change bed time and check the pay
			babysitter2.setBedTime(11);
			employeeService.setEmployee(babysitter2);
			assertEquals(96, employeeService.calculatePay());
		}

		@Test
		public void testCalculatePayIntermediate() {
			//test when bed time is earlier than start time
			employeeService = new EmployeeService(new Babysitter(10, 9, 4));
			assertNotNull(employeeService);
			assertEquals(80, employeeService.calculatePay());
			
			//test when start time is after midnight
			employeeService.setEmployee(new Babysitter(12, 9, 4));
			assertEquals(64, employeeService.calculatePay());
			
			//test when bedtime is later than midnight
			employeeService.setEmployee(new Babysitter(8, 1, 4));
			assertEquals(112, employeeService.calculatePay());
		}
		
		@Test
		public void testCalculatePayAdvance() {
			//test when start time and end time are same, should be 0 payment
			employeeService = new EmployeeService(new Babysitter(3, 9, 3));
			assertEquals(0, employeeService.calculatePay());
			
			//test when start time is same as midnight time
			employeeService.setEmployee(new Babysitter(12, 12, 4));
			assertEquals(64, employeeService.calculatePay());
			
			//test when bedtime and start time is same
			employeeService.setEmployee(new Babysitter(8, 8, 4));
			assertEquals(96, employeeService.calculatePay());
			
			//test when end time is earlier than start time (invalid time)
			employeeService.setEmployee(new Babysitter(3, 12, 1));
			assertEquals(0, employeeService.calculatePay());
			
			//test when startime is after midnight and bed time before midnight
			employeeService.setEmployee(new Babysitter(1, 10, 3));
			assertEquals(32, employeeService.calculatePay());
			
			//test when end time is earlier than bed time
			employeeService.setEmployee(new Babysitter(5, 10, 9));
			assertEquals(48, employeeService.calculatePay());
			
			//test when bedtime after midnight and endtime before midnight
			employeeService.setEmployee(new Babysitter(5, 1, 10));
			assertEquals(60, employeeService.calculatePay());
			
			
		}
}
