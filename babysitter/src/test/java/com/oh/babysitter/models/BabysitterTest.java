package com.oh.babysitter.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for Babysitter
 * 
 * @author Shiva Acharya
 *
 */

class BabysitterTest {
	
	private Babysitter babysitter;
	
	//setup
	@BeforeEach
	public void setUp() {
		babysitter = new Babysitter();
	}

	//test no argument constructor
	@Test
	public void testNoArgumentConstructor() {
		
		assertNotNull(babysitter);
		assertEquals(0, babysitter.getStartTime());
		assertEquals(0, babysitter.getBedTime());
		assertEquals(0, babysitter.getEndTime());
	}
	
	//test arguments constructor
	@Test
	public void testArgsConstructor() {
		
		babysitter = new Babysitter(1, 2, 3);
		assertNotNull(babysitter);
		
		assertEquals(1, babysitter.getStartTime());
		assertEquals(2, babysitter.getBedTime());
		assertEquals(3, babysitter.getEndTime());
		
		//test one more time
		babysitter = new Babysitter(5, 10, 18);
		assertNotNull(babysitter);
		
		assertEquals(5, babysitter.getStartTime());
		assertEquals(10, babysitter.getBedTime());
		assertEquals(18, babysitter.getEndTime());
	
	}
	
	//test getters and setters and to string
	@Test
	public void testGettersAndSetters() {

		assertNotNull(babysitter);
		babysitter.setStartTime(1);
		babysitter.setBedTime(11);
		babysitter.setEndTime(5);
		assertEquals(1, babysitter.getStartTime());
		assertEquals(11, babysitter.getBedTime());
		assertEquals(5, babysitter.getEndTime());
		
		//test one more time
		babysitter.setStartTime(3);
		babysitter.setBedTime(8);
		babysitter.setEndTime(7);
		
		assertEquals(3, babysitter.getStartTime());
		assertEquals(8, babysitter.getBedTime());
		assertEquals(7, babysitter.getEndTime());
		
		//check few values to see toString method contains it
	
		assertTrue(babysitter.toString().contains("3"));
		assertTrue(babysitter.toString().contains("bedTime=8"));
	}

}
