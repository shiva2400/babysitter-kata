package com.oh.babysitter.models;

public class Babysitter implements Employee {
	
	//instance variables for Babysitter
	private int startTime;
	private int bedTime;
	private int endTime;

	//argumentative constructor 
	public Babysitter(int startTime, int bedTime, int endTime) {
		this.startTime = startTime;
		this.bedTime = bedTime;
		this.endTime = endTime;
	}
	
	//empty constructor 
	public Babysitter() {
		
	}
	
	/**
	 * sets the start time
	 * 
	 * @param the start time
	 */
	@Override
	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	//returns the start time
	@Override
	public int getStartTime() {
		return this.startTime;
	}

	//sets the bed time
	@Override
	public void setBedTime(int bedTime) {
		this.bedTime = bedTime;

	}

	//returns the bed time
	@Override
	public int getBedTime() {
		return this.bedTime;
	}

	//sets the end time
	@Override
	public void setEndTime(int endTime) {
		this.endTime = endTime;

	}

	//returns the end time
	@Override
	public int getEndTime() {
		return endTime;
	}

	//toString method for testing purpose on console
	@Override
	public String toString() {
		return "Babysitter [startTime=" + startTime + ", bedTime=" + bedTime + 
				", endTime=" + endTime + "]";
	}
	
	

}
