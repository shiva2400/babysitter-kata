package com.oh.babysitter.models;
/**
 * Defines the interface for employee, Babysitter can easily be Pluggable 
 * if we happen to work on same scenario for other employee type
 * 
 * @author Shiva Acharya
 * @version 1.0
 *
 */
public interface Employee {
	
	/**
	 * sets the start time for the employee
	 * 
	 * @param startTime the daily start time for the employee
	 */
	void setStartTime(int startTime);
	
	/**
	 * Returns the start time of the employee(Babysitter for now)
	 * 
	 * @return the start time
	 */
	int getStartTime();
	
	/**
	 * sets the bed time for the employee
	 * 
	 * @param startTime the daily bed time for the employee
	 */
	void setBedTime(int bedTime);
	
	/**
	 * Returns the bed time of the employee(Babysitter for now)
	 * 
	 * @return the bed time
	 */
	int getBedTime();
	
	/**
	 * sets the end time for the employee
	 * 
	 * @param startTime the daily end time for the employee
	 */
	void setEndTime(int endTime);
	
	/**
	 * Returns the end time of the employee(Babysitter for now)
	 * 
	 * @return the end time
	 */
	int getEndTime();

}
