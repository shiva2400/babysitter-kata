/**
 * 
 */
package com.oh.babysitter.service;

import com.oh.babysitter.models.Employee;

/**
 * Employee Service class that will calculate payment for any employee
 * 
 * @author Shiva Acharya
 *
 */
public class EmployeeService {

	// employee object that will hold (Babysitter in this project)
	private Employee employee;

	// Constructor that will take employee object as parameter
	public EmployeeService(Employee employee) {
		this.employee = employee;
	}

	// returns the employee object
	public Employee getEmployee() {
		return employee;
	}

	// sets the employee object
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * method to calculate the nightly charge
	 * 
	 * calculates the total pay for the employee that is initialized in the
	 * constructor returns 0, if the employee object is null
	 * 
	 * @return total pay
	 */
	public int calculatePay() {

		// initialize the totalPay to 0
		int totalPay = 0;

		// if employee object is null, return 0
		if (this.employee == null)
			return totalPay;

		// if Time entry is invalid, return 0
		boolean result = isValidTimeEntry(this.employee.getStartTime(), this.employee.getBedTime(),
				this.employee.getEndTime());
		if (!result)
			return totalPay;

		totalPay = calculatePayHelper(this.getEmployee());

		return totalPay;

	}

	/**
	 * helper method for calculating pay
	 * 
	 * @param employee2 the employee object passed
	 * @return total payment
	 */
	private int calculatePayHelper(Employee employee2) {

		final int START_TO_BED_RATE = 12; // rate for start time to bed
		final int BED_TO_MIDNIGHT_RATE = 8;// rate for bed time to midnight
		final int MIDNIGHT_TO_END_RATE = 16; // rate for midnight to end
		final int MIDNIGHT_TIME = 12; // midnight time
		final int TIME_CONVERSION_NUMBER = 5; // convert any number below 5 because those will be AM hours
		int totalPay = 0;

		// convert the time after 12 am to 13, 14 and so on
		int startTime = employee2.getStartTime();
		int bedTime = employee2.getBedTime();
		int endTime = employee2.getEndTime();

		startTime = startTime < TIME_CONVERSION_NUMBER ? startTime += 12 : startTime;
		bedTime = bedTime < TIME_CONVERSION_NUMBER ? bedTime += MIDNIGHT_TIME : bedTime;
		endTime = endTime < TIME_CONVERSION_NUMBER ? endTime += MIDNIGHT_TIME : endTime;

		// return 0 if end time is earlier than start time after coversion
		if (endTime < startTime)
			return totalPay;

		if (startTime >= MIDNIGHT_TIME) {
			totalPay = (endTime - startTime) * MIDNIGHT_TO_END_RATE;
		} else if (startTime >= bedTime) {
			totalPay = ((endTime - MIDNIGHT_TIME) * MIDNIGHT_TO_END_RATE)
					+ (MIDNIGHT_TIME - startTime) * BED_TO_MIDNIGHT_RATE;
		} else if (bedTime >= startTime && bedTime >= MIDNIGHT_TIME && bedTime < endTime) {
			totalPay = ((endTime - MIDNIGHT_TIME) * MIDNIGHT_TO_END_RATE)
					+ ((MIDNIGHT_TIME - startTime) * START_TO_BED_RATE);
		} else if (endTime < MIDNIGHT_TIME && bedTime <= endTime &&
				bedTime >= startTime) {
			totalPay = ((endTime - bedTime) * BED_TO_MIDNIGHT_RATE) 
					+ ((bedTime - startTime) * START_TO_BED_RATE);
		} else if (endTime <= bedTime && startTime <= bedTime ) {
			totalPay = (endTime - startTime) * START_TO_BED_RATE;
			
		}else {
			totalPay = ((bedTime - startTime) * START_TO_BED_RATE) + ((MIDNIGHT_TIME - bedTime) * BED_TO_MIDNIGHT_RATE)
					+ ((endTime - MIDNIGHT_TIME) * MIDNIGHT_TO_END_RATE);
		}

		return totalPay;
	}

	/**
	 * 
	 * @param startTime the start time
	 * @param bedTime   the bed time
	 * @param endTime   the end time
	 * @return true if time entry is valid and false if time entry is invalid
	 * 
	 *         This method should be private but I'making public for testing
	 *         purposes
	 */

	public boolean isValidTimeEntry(int startTime, int bedTime, int endTime) {

		// return false if any time is not within 5 - 4 inclusive (Babysitter can only
		// work from 5 to 4)

		if ((startTime < 1 || startTime > 12) || (bedTime < 1 || bedTime > 12) || (endTime < 1 || endTime > 12)) {
			return false;
		}

		// invalid time when end time and start time are same
		if (startTime == endTime)
			return false;

		return true;
	}

}
