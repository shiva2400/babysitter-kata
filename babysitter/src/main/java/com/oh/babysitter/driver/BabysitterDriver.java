package com.oh.babysitter.driver;

import com.oh.babysitter.models.Babysitter;
import com.oh.babysitter.service.EmployeeService;

/**
 * Driver class for Babysitter class
 * 
 * @author Shiva Acharya
 *
 */
public class BabysitterDriver {

	public static void main(String[] args) {
		
		EmployeeService service = new EmployeeService(new Babysitter(0, 0, 0));
		
		System.out.println("====================================Pay Calculator==============================");
		System.out.print(service.getEmployee().toString());
		System.out.println("\t Payment = $" + service.calculatePay() + "\n");
		
		service.setEmployee(new Babysitter(-3, 5, 7));
		System.out.print(service.getEmployee().toString());
		System.out.println("\t Payment = $" + service.calculatePay() + "\n");
		
		service.setEmployee(new Babysitter(10, 5, 17));
		System.out.print(service.getEmployee().toString());
		System.out.println(" Payment = $" + service.calculatePay() + "\n");
		
		service.setEmployee(new Babysitter(5, 11, 4));
		System.out.print(service.getEmployee().toString());
		System.out.println("\t Payment = $" + service.calculatePay() + "\n");
		
		service.setEmployee(new Babysitter(11, 10, 4));
		System.out.print(service.getEmployee().toString());
		System.out.println(" Payment = $" + service.calculatePay() + "\n");
		
		service.setEmployee(new Babysitter(11, 11, 4));
		System.out.print(service.getEmployee().toString());
		System.out.println("Payment = $" + service.calculatePay() + "\n");
		
		service.setEmployee(new Babysitter(1, 11, 3));
		System.out.print(service.getEmployee().toString());
		System.out.println(" Payment = $" + service.calculatePay() + "\n");
		
		service.setEmployee(new Babysitter(10, 3, 4));
		System.out.print(service.getEmployee().toString());
		System.out.println(" Payment = $" + service.calculatePay() + "\n");
		
		service.setEmployee(new Babysitter(5, 1, 10));
		System.out.print(service.getEmployee().toString());
		System.out.println(" Payment = $" + service.calculatePay() + "\n");

	}

}
